/// CreateBullet(x, y, bulletType, spd, direction)
/// @description InitializeBullet(x, y, bulletType, spd, direction)
/// @param x
/// @param y
/// @param bulletType
/// @param speed
/// @param direction
// Creates specified bullet at location with an initial speed and direction

xPos = argument0;
yPos = argument1;
bulletType = argument2;
spd = argument3;
dir = argument4;

bullet = instance_create(argument0, argument1, argument2);
bullet.direction = dir;
bullet.image_angle = dir;
bullet.speed = spd;

return bullet;