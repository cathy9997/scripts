/// @desc WindowCenterAndResize
/// @param width
/// @param height
/// @param scale

var width = argument0;
var height = argument1;
var scale = argument2;

window_set_size(width * scale, height * scale);

alarm[0] = 1; // Center window 1 frame after setting window size

surface_resize(application_surface, width * scale, height * scale);