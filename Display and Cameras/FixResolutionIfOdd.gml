/// @desc FixResolutionIfOdd
/// @param width
/// @param height

width = argument0;
height = argument1;

if(width % 2 == 1)
{
    width++;
}
if(height % 2 == 1)
{
    height++;
}