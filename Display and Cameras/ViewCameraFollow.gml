/// @desc ViewCameraFollow(object, viewCamera, cameraSpeed)
/// @param object
/// @param viewCamera
/// @param cameraSpeed

var object = argument0;
var viewCamera = argument1;
var cameraSpeed = argument2;

var cameraWidth = camera_get_view_width(viewCamera);
var cameraHeight = camera_get_view_height(viewCamera);

var destinationX = clamp(object.x - (cameraWidth / 2), 0, room_width - cameraWidth);
var destinationY = clamp(object.y - (cameraHeight /  2), 0, room_height - cameraHeight);

var currentX = camera_get_view_x(viewCamera);
var currentY = camera_get_view_y(viewCamera);

var moveX = lerp(currentX, destinationX, cameraSpeed);
var moveY = lerp(currentY, destinationY, cameraSpeed);

camera_set_view_pos(viewCamera, moveX, moveY);