/// @desc WindowCalculateMaxScale(width, height)
/// @param width
/// @param height

var width = argument0;
var height = argument1;

var maxScale;
maxScale = min(floor(display_get_width() / idealWidth), floor(display_get_height() / idealHeight));
if(idealHeight * maxScale == display_get_height())
{
    maxScale--;
}
return maxScale;