/// @desc MouseClickOnButton(buttonArray, mouseButton)
/// @param buttonArray
/// @param mouseButton
var buttonArray = argument0;
var mouseButton = argument1;
/* Intended for use with following enum declared
enum button
{
    x1,
    y1, 
    x2,
    y2,
    text
}
*/
return MouseClickInRegion(buttonArray[button.x1], buttonArray[button.y1], buttonArray[button.x2], buttonArray[button.y2], mouseButton);