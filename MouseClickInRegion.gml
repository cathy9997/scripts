/// @desc MouseClickInRegion(x1, y1, x2, y2, mouseButton)
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param mouseButton

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var mouseButton = argument4;

if(mouse_check_button_pressed(mouseButton))
{
	if(mouse_x >= x1 && mouse_x <= x2)
	{
	    if(mouse_y >= y1 && mouse_y <= y2)
	    {
	        return true;
	    }
	}
}
return false;