/// @desc OnScreen(instance, camera)
/// @param instance
/// @param camera
if(argument_count == 0)
{
    return false;
}
var instance = argument[0];

if(argument_count >= 2)
{
    var camera = argument[1];
}
else
{
    var camera = view_camera[0];
}

if(x < camera_get_view_x(camera) || x > (camera_get_view_x(camera) + camera_get_view_width(camera)))
{
    return false;
}

if(y < camera_get_view_y(camera) || y > (camera_get_view_y(camera) + camera_get_view_height(camera)))
{
    return false;
}
return true;
