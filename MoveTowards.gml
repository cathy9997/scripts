/// @description MoveTowards(start, goal, amount)
/// @param start
/// @param goal
/// @pamam amount
// move start towards goal by amount. If start goes past goal, returns goal
start = argument0;
goal = argument1;
amount = argument2;

if(start < goal)
{
    start += amount;
    if(start > goal)
    {
        return goal;
    }
    return start;
}
else //(start > goal)
{
    start -= amount;
    if(start < goal)
    {
        return goal;
    }
    return start;
}