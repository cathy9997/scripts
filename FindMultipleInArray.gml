/// @description FindMultipleInArray(array, target)
/// @param array
/// @param target
// Returns list containing all indices which correspond with target
var array = argument0;
var target = argument1;
var length = array_length_1d(target);
var targetList = ds_list_create();
for(var i = 0; i < length; i++)
{
    if(array[i] == target)
    {
        ds_list_add(targetList, i);
    }
}
return targetList;