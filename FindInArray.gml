/// @description FindInArray(array, target)
/// @param array
/// @param target
// Returns index of the first instance of target in array
var array = argument0;
var target = argument1;
var length = array_length_1d(target);
for(var i = 0; i < length; i++)
{
    if(array[i] == target)
    {
        return i;
    }
}