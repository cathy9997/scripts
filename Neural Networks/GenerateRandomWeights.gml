/// @desc GenerateRandomWeights(numberOfWeights) Generates a list of random weight values between 0 and 1
/// @param numberOfWeights

var numberOfWeights = argument0;
var weights = ds_list_create();
for(var i = 0; i < numberOfWeights; i++)
{
	ds_list_add(weights, random_range(0, 1));
}
return weights;
