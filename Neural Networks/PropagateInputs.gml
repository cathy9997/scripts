/// @desc PropagateInputs(propagateFrom) Takes a list of neurons and returns a list of their output values as input values for new neuron
/// @param propagateFrom

var propagateFrom = argument0;
var inputs = ds_list_create();
for(var j = 0; j < ds_list_size(propagateFrom); j++)
{
	var inputNeuron = propagateFrom[|j];
	ds_list_add(inputs, NeuronGetOutput(inputNeuron));
}
return inputs;