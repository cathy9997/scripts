/// @desc Sigmoid(value)
/// @param value
#macro e 2.71828
var value = argument0;
var result = 1/(1 + power(e, -2 * value));
return result;