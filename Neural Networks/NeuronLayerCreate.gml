/// @desc NeuronLayerCreate(numberOfNeurons, previousLayer);
/// @param numberOfNeurons
/// @param previousLayer

var numberOfNeurons = argument0;
var previousLayer = argument1;

var neuronLayer = ds_list_create();
for(var i = 0; i < numberOfNeurons; i++)
{
	var inputs = PropagateInputs(previousLayer);
	var numberOfWeights = ds_list_size(previousLayer);
	var neuronInputWeights = GenerateRandomWeights(numberOfWeights);
	var neuron = NeuronCreate(inputs, neuronInputWeights);
	NeuronSetOutput(neuron, NeuronCalculateOutput(neuron));
	NeuronSetOutput(neuron, NeuronCalculateOutput(neuron));
	AddNeuronToList(neuronLayer, neuron);
}
return neuronLayer;