/// @desc IsNeuron(neuron)
/// @desc neuron
var neuron = argument0;

if(ds_exists(neuron, ds_type_map))
{
	// Check for "Inputs"
	var inputs = ds_map_find_value(neuron, "Inputs");
	// Check for "Weights"
	var weights = ds_map_find_value(neuron, "Weights");
	// Check for "Output"
	var output = ds_map_find_value(neuron, "Output");
	if(is_undefined(inputs) || is_undefined(weights) || is_undefined(output))
	{
		return false;
	}
	else
	{
		return true;
	}
}
else
{
	return false;
}