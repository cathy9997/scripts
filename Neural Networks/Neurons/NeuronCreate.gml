/// @desc NeuronCreate(neuronInputs, neuronInputWeights)
/// @param neuronInputs
/// @param neuronInputWeights
var neuron = ds_map_create();
if(argument_count == 2)
{
	var inputs = argument[0];
	var neuronInputWeights = argument[1];
	var output = 0;
}
else
{
	var inputs = -1;
	var neuronInputWeights = -1;
	var output = 0;
}

ds_map_add(neuron, "Inputs", inputs);
ds_map_add(neuron, "Weights", neuronInputWeights);
ds_map_add(neuron, "Output", output);
return neuron;