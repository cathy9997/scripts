/// @desc NeuronGetOutput(neuron)
/// @param neuron
var neuron = argument0;

if(IsNeuron(neuron))
{
	return neuron[? "Output"];
}
else
{
	show_debug_message("Error getting neuron output: This is not a neuron");
}