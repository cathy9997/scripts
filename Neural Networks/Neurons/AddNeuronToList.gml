/// @description AddNeuronToList(list, neuron)
/// @param list
/// @param neuron
var list = argument0;
var neuron = argument1;
if(ds_exists(list, ds_type_list))
{
	if(IsNeuron(neuron))
	{
		ds_list_add(list, neuron);
		ds_list_mark_as_map(list, ds_list_size(list) - 1);
	}
	else
	{
		show_debug_message("Error Adding Neuron to list: This is not a neuron");
	}
}
else 
{
	show_debug_message("Error Adding Neuron to list: List does not exist");
}
	