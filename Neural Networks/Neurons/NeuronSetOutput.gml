/// @desc NeuronSetOutput(neuron, output)
/// @param neuron
/// @param output
var neuron = argument0;
var output = argument1;
if(IsNeuron(neuron))
{
	neuron[? "Output"] = output;
}
else
{
	show_debug_message("Error Setting Neuron Output: This is not a neuron");
}