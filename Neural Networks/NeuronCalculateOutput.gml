/// @desc NeuronCalculateOutput(neuron)
/// @param neuron
var neuron = argument0;
var weightedSum = 0;
var inputsList = neuron[? "Inputs"];
var weightsList = neuron[? "Weights"];
var numberOfInputs = ds_list_size(inputsList);
for(var i = 0; i < numberOfInputs; i++)
{
	weightedSum += inputsList[| i] * weightsList[| i];
}
var output = Sigmoid(weightedSum);
return output;