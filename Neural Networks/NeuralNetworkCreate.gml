/// @desc NeuralNetworkCreate(inputNumber, inputLayer, outputNumber)
/// @param inputNumber
/// @param inputLayer
/// @param outputNumber

var inputNumber = argument0;
var inputLayer = argument1;
var outputNumber = argument2;
var hiddenNumber = ceil(inputNumber * (2 / 3)) + 2;
var hiddenLayer = NeuronLayerCreate(hiddenNumber, inputLayer);
var outputLayer = NeuronLayerCreate(outputNumber, hiddenLayer);
var neuralNetwork = ds_map_create();
ds_map_add_list(neuralNetwork, "InputLayer", inputLayer);
ds_map_add_list(neuralNetwork, "HiddenLayer", hiddenLayer);
ds_map_add_list(neuralNetwork, "OutputLayer", outputLayer);
return neuralNetwork;