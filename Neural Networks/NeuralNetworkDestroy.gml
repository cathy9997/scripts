/// @desc NeuralNetworkDestroy(neuralNetwork)
/// @param neuralNetwork

var neuralNetwork = argument0;
ds_map_destroy(neuralNetwork);