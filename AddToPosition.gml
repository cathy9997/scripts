/// @description AddToPosition(value, direction) Add a vector current position
/// @param value
/// @param direction

var value = argument0;
var dir = argument1;

x += lengthdir_x(value, dir);
y += lengthdir_x(value, dir);